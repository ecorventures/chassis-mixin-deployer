'use strict'

const ShortBus = require('shortbus')
const download = require('./Downloader')
const fs = require('fs')
const path = require('path')
const EventEmitter = require('events').EventEmitter
const gitRoot = 'https://api.github.com/repos/'

/**
 * @class Repo
 * Represents a Github Repository.
 */
class Repo extends EventEmitter {
  constructor (repo, token) {
    super()

    Object.defineProperties(this, {
      account: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: repo.split('/')[0]
      },
      repo: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: repo.split('/')[1]
      },
      fullrepo: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: repo
      },
      token: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: token
      },
      headers: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: {
          'user-agent': repo.split('/')[0] + ' ' + repo.split('/')[1] + ' release manager',
          'Authorization': 'token ' + token
        }
      }
    })
  }

  get repository () {
    return this.fullrepo
  }

  notify (msg) {
    this.emit('status', msg)
  }

  downloadFile (file, callback) {
    const me = this

    download(gitRoot + this.repository + '/git/refs/heads/master', this.headers, function (data) {
      download(data.object.url, me.headers, function (commitdata) {
        download(commitdata.tree.url, me.headers, function (treedata) {
          const dir = treedata.tree
          const dfile = dir.filter(function (asset) {
            return asset.path.toLowerCase() === file
          })

          if (dfile.length === 0) {
            throw new Error(dfile + ' not found in ' + me.repository)
          }

          download(dfile[0].url, me.headers, function (filedata) {
            me.notify('Downloaded ' + file)
            callback(new Buffer.from(filedata.content, 'base64').toString())
          })
        })
      })
    })
  }

  getContributors (callback) {
    const me = this
    this.notify('Retrieving contributor list.')
    download(gitRoot + this.repository + '/collaborators', this.headers, function (people) {
      me.notify('Contributor list acquired.')
      callback(people.map(function (person) {
        return {
          name: person.login,
          url: person.html_url
        }
      }))
    })
  }
}

module.exports = Repo
