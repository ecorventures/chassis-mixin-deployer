'use strict'

const ShortBus = require('shortbus')
const download = require('./Downloader')
const fs = require('fs')
const path = require('path')
const GitHubApi = require("github")
const EventEmitter = require('events').EventEmitter

/**
 * @class Release
 * Represents a Github Release.
 */
class Release extends EventEmitter {
  constructor (repo, token) {
    super()

    Object.defineProperties(this, {
      account: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: repo.split('/')[0]
      },
      repo: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: repo.split('/')[1]
      },
      fullrepo: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: repo
      },
      token: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: token
      },
      headers: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: {
          'user-agent': repo.split('/')[0] + ' ' + repo.split('/')[1] + ' release manager',
          'Authorization': 'token ' + token
        }
      },
      latestVersion: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: null
      },
      filecount: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: 0
      },
      assets: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: []
      },
      github: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: new GitHubApi({
          // optional
          debug: false,
          protocol: 'https',
          host: 'api.github.com', // should be api.github.com for GitHub
          // pathPrefix: "/api/v3", // for some GHEs; none for GitHub
          timeout: 5000,
          headers: {
            'user-agent': repo.split('/')[0] + ' ' + repo.split('/')[1] + ' release manager',
            'Authorization': 'token ' + token
          },
          followRedirects: false, // default: true; there's currently an issue with non-get redirects, so allow ability to disable follow-redirects
        })
      }
    })
  }

  get repository () {
    return this.fullrepo
  }

  get version () {
    return this.latestVersion || 'Unknown'
  }

  notify (msg) {
    this.emit('status', msg)
  }

  authenticate () {
    this.github.authenticate({
      type: 'oauth',
      token: this.token
    })
  }

  clearFiles (files, callback) {
    const me = this
    const tasks = new ShortBus()

    tasks.add('Authenticate', function () {
      me.notify('Authenticating with Github.')
      me.authenticate()
    })

    files.forEach(function (file) {
      tasks.add('Removing ' + file + ' from v' + me.version + ' release.', function (next) {
        const task = this
        const asset = me.assets.filter(function (asset) {
          return asset.name === file
        })[0]
        me.github.repos.deleteAsset({
          user: me.account,
          repo: me.repo,
          id: asset.id
        }, function (err) {
          if (err) {
            throw err
          }
          me.notify(task.number + '\) ' + task.name)
          setTimeout(next, 1000)
        })
      })
    })

    tasks.on('complete', callback)
    tasks.process(true)
  }

  /**
   * @method downloadLatest
   * Download the latest release files.
   * @param  {string} dir
   * The output directory.
   * @param [function] callback
   */
  downloadLatest (dir, callback) {
    const me = this
    const tasks = new ShortBus()

    this.latestReleaseFileList(function (assets) {
      me.assets = assets
      if (assets.length === 0) {
        return callback()
      }

      assets.forEach(function (asset) {
        me.notify('Queued download of ' + asset.name + ' (' + asset.size + ')')
        tasks.add('Download ' + asset.name, function (next) {
          const task = this
          download(asset.url, me.headers, function (content) {
            me.notify(task.number + '\) ' + task.name)
            if (typeof content === 'object') {
              console.log('BREAKPOINT 1')
              content = JSON.stringify(content)
            } else {
              console.log('BREAKPOINT 2')
              content = content.toString()
            }
            console.log('BREAKPOINT 3')
            try {
              console.log('BREAKPOINT 4')
              fs.writeFileSync(path.join(dir, path.basename(asset.url)), content, {
                encoding: 'utf8'
              })
              console.log('BREAKPOINT 5')
              setTimeout(next, 1500)
            } catch (e) {
              console.log('BREAKPOINT 6')
              console.log(content)
              console.log('BREAKPOINT 7')
              throw e
            }
          })
        })
      })

      tasks.on('complete', function () {
        // Check to make sure all the files were downloaded
        const downloadedFiles = fs.readdirSync(dir)
        if (downloadedFiles.length !== assets.length) {
          throw new Error('Files downloaded: ' + downloadedFiles.length + ', expected: ' + assets.length)
        }

        me.notify('Downloads complete.')

        callback()
      })

      tasks.process(true)
    })
  }

  /**
   * @method latestReleaseFileList
   * Retrieves a list of the latest files released.
   * @param {function} callback
   * The callback to execute when the list is ready.
   * @param {array} callback.assets
   * The assets available in the release. The array contains
   * objects of the following specificiation:
   * ```
   * {
   *   name: <name of file>,
   *   size: <size in kb>,
   *   url: <browser download URL>
   * }
   * ```
   */
  latestReleaseFileList (callback) {
    const me = this

    this.notify('Retrieving releases for ' + this.fullrepo)

    download('https://api.github.com/repos/' + this.fullrepo + '/releases/latest', this.headers, function (body) {
      me.latestVersion = body.tag_name
      console.log(body)
      me.filecount = body.assets.length

      me.notify('Manifest retrieved for v' + me.latestVersion + '. ' + me.filecount + ' assets in this release.')

      me.assets = body.assets.map(function (asset) {
        return {
          name: asset.name,
          size: (Math.ceil(asset.size/1000)) + 'kb',
          url: asset.browser_download_url,
          id: asset.id
        }
      })

      callback(me.assets)
    })
  }
}

module.exports = Release
