'use strict'

require('localenvironment')
const fs = require('fs')
const path = require('path')
const GithubRelease = require('./lib/Release')
const GithubCDN = require('./lib/CDN')
const GithubRepo = require('./lib/Repository')
const npm = require('./lib/npm')
const https = require('https')
const os = require('os')
const del = require('rimraf')
const output = path.join(os.tmpdir(), 'output')
const MustHave = require('musthave')
const cp = require('child_process')
const mh = new MustHave({
  throwOnError: false
})

// Make sure all of the configuration options are available.
if (!mh.hasAll(process.env, 'source_owner', 'source_repository', 'target_owner', 'target_repository', 'target_token', 'npm_module', 'npm_module_files', 'npm_user')) {
  console.log('Missing', mh.missing.join(', '))
  process.exit(1)
}

process.env.npm_module_files = process.env.npm_module_files.split(',').map(function (filename) {
  return filename.trim()
})

// Clean the working directory
try {
  fs.accessSync(output, fs.F_OK)
  del.sync(output)
} catch (e) {}
fs.mkdirSync(output)

// Retrieve the latest code
const start = new Date().getTime()
console.log('Retrieving new release...')

// Identify the latest release repo & CDN repo
const Release = new GithubRelease(process.env.source_owner + '/' + process.env.source_repository, process.env.source_token)
const CDN = new GithubCDN(process.env.target_owner + '/' + process.env.target_repository, process.env.target_directory, process.env.source_token)
const Source = new GithubRepo(process.env.source_owner + '/' + process.env.source_repository, process.env.source_token)
const NPM = new npm(process.env.npm_user, process.env.npm_token)
const mainscript = (process.env.npm_module_main || 'index.js')

// Output status messages from the release
Release.on('status', function (msg) {
  console.log('[' + Release.repository.toUpperCase() + ' GITHUB RELEASE] ' + msg)
})

// Output status messages from the CDN repo processes.
CDN.on('status', function (msg) {
  console.log('[' + CDN.repository.toUpperCase() + ' REPO] ' + msg)
})

// Output NPM source status
Source.on('status', function (msg) {
  console.log('[' + Source.repository.toUpperCase() + ' GITHUB REPO] ' + msg)
})

// Output NPM status
NPM.on('status', function (msg) {
  console.log('[NPM] ' + msg)
})

// 1. Download the latest release
Release.downloadLatest(output, function () {
  const files = fs.readdirSync(output)

  // Identify JS files
  const js = files.filter(function (file) {
    return path.extname(file) === '.js'
  }).map(function (file) {
    return path.join(output, file)
  })

  // Identify the map files
  const maps = files.filter(function (file) {
    return path.extname(file) === '.map'
  })
  let mapfiles = maps.map(function (file) {
    return path.join(output, file)
  })

  // Clear the CDN Repo
  CDN.clearReleaseFiles(function () {
    CDN.uploadReleaseFiles(js, function () {
      if (maps.length > 0) {
        CDN.publishGithubPagesFiles(mapfiles, path.join(process.env.target_sourcemaps_dir, Release.version), function () {
          Release.clearFiles(maps, function () {
            publish(function () {
              console.log('Sourcemaps updated.\nSUCCESS!')
            })
          })
        })
      } else {
        publish(function () {
          console.log('No sourcemaps found.\nSUCCESS!')
        })
      }
    })
  })

  const publish = function (callback) {
    // Generate the package.json
    NPM.set('name', process.env.npm_module.toLowerCase())
    NPM.set('version', Release.version)
    NPM.set('license', process.env.npm_license || 'BSD3')
    NPM.set('main', mainscript)

    if (process.env.npm_module_description) {
      NPM.set('description', process.env.npm_module_description)
    }

    if (process.env.npm_module_keywords) {
      NPM.set('keywords', process.env.npm_module_keywords.split(','))
    }

    if (process.env.npm_module_author) {
      NPM.set('author', process.env.npm_module_author)
    }

    // Download the assets
    let npmassets = fs.readdirSync(output).filter(function (asset) {
      const isLicenseFile = path.basename(asset).toLowerCase().substr(0, 7) === 'license'
      return isLicenseFile || (process.env.npm_module_files || []).indexOf(path.basename(asset)) >= 0
    })

    if (npmassets.length === 0) {
      throw new Error('No assets were specified/available for deployment to NPM.')
    }

    NPM.readme = 'Please visit [' + Source.repository + '](https://github.com/' + Source.repository + ') for information.'

    Source.getContributors(function (people) {
      if (people.length > 0) {
        NPM.set('contributors', people)
      }

      Source.downloadFile(mainscript, function (content) {
        fs.writeFileSync(path.join(output, mainscript), content, {encoding: 'utf8'})
        npmassets.push(mainscript)

        npmassets.forEach(function (filepath) {
          NPM.addFile(path.join(output, filepath), filepath.replace(output, ''))
        })

        NPM.build(function () {
          NPM.publish(function (message) {
            if (message) {
              throw new Error(message)
            } else {
              console.log('Processing complete.')
            }
          })
        })
      })
    })
  }
  // // Upload static assets for
  // if (maps.length > 0) {
  //   CDN.uploadHostedFiles(mapfiles, path.join(process.env.target_sourcemaps_dir, Release.version), function () {
  //     // Remove unncessary release files once they're on the hosted github page.
  //     Release.clearFiles(maps, function () {
  //       // Remove old assets
  //       CDN.clear(function () {
  //         // Upload new assets
  //         CDN.upload(output, function () {
  //           console.log('All done. (sourcemaps supported)')
  //         })
  //       })
  //     })
  //   })
  // } else {
  //   CDN.clear(function () {
  //     // Upload new assets
  //     CDN.upload(output, function () {
  //       console.log('All done.')
  //     })
  //   })
  // }
})

// process.on('beforeExit', function (exitcode) {
//   del.sync(output)
//
//   // Hipchat Notification
//   if (mh.hasAll(process.env, 'hipchat_token', 'hipchat_room')) {
//     let hipchat = new HipChat({
//       auth_token: process.env.hipchat_token,
//       room_id: process.env.hipchat_room,
//       from: process.env.hipchat_from || 'CDN Deployer',
//       format: 'html'
//     })
//
//     let content = '<a href="https://github.com/' + process.env.source_owner
//       + '/' + process.env.source_repository + '/releases/tag/' + Release.version
//       + '">' + process.env.source_owner + '/' + process.env.source_repository
//       + ' ' + Release.version + ' release' + '</a>&nbsp;&#x2192;&nbsp;'
//       + '<a href="https://github.com/' + process.env.target_owner + '/'
//       + process.env.target_repository + '/tree/master/' + process.env.target_directory
//       + '">' + process.env.target_owner + '/' + process.env.target_repository + '/'
//       + process.env.target_directory + '</a>'
//
//     console.log('Sending Hipchat Notice.')
//
//     if (exitcode === 0) {
//       content += '<br/><strong>Deployed Successfully.</strong>'
//       hipchat.success(content, true)
//     } else {
//       content += '<br/><strong>Failed to deploy.</strong>'
//       hipchat.error(content, true)
//     }
//   }
//
//   // Webhook
//   if (exitcode === 0 && process.env.hasOwnProperty('webhook')) {
//     request.post({
//       url: process.env.webhook,
//       header: {
//         'user-agent': 'Github to CDN Redeployer'
//       }
//     }, function (err, res, body) {
//       if (err) {
//         console.log(err)
//         process.exit(1)
//       }
//       if (res.statusCode !== 200) {
//         throw new Error('Webhook Failed: (' + res.statusCode + ') ' + body)
//       }
//       process.exit(0)
//     })
//   }
// })

// process.on('uncaughtException', function (err) {
//   if (mh.hasAll(process.env, 'hipchat_token', 'hipchat_room')) {
//     if (!err.message || err.message.indexOf('cannot publish') < 0) {
//       let hipchat = new HipChat({
//         auth_token: process.env.hipchat_token,
//         room_id: process.env.hipchat_room,
//         from: process.env.hipchat_from || 'CDN Deployer',
//         title: 'Uncaught Exception',
//         format: 'html'
//       })
//
//       let content = 'An error occurred while copying ' + process.env.source_owner + '/' + process.env.source_repository + ' release to '
//         + process.env.target_owner + '/' + process.env.target_repository + ' (' + process.env.target_directory + ' directory) for CDN use:<br/>'
//         + ':<br/><strong>' + err.message + '</strong><br/><pre>' + err.stack + '</pre>'
//
//       hipchat.error(content, true, function () {
//         process.exit(1)
//       })
//     }
//   }
// })
